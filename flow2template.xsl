<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes" />
    
    <xsl:template match="/">        
        <template encoding-version="1.2">
            <name><xsl:value-of select="processGroup/name"/></name>
            <description></description>
            <snippet>
                <xsl:apply-templates select="processGroup"/>
            </snippet>
        </template>        
    </xsl:template>
    
    <xsl:template match="processGroup">
        <processGroups>
            <id>
                <xsl:call-template name="id">
                    <xsl:with-param name="id" select="id" />
                </xsl:call-template>
            </id>
            <xsl:if test="../id">
                <parentGroupId>
                    <xsl:call-template name="id">
                        <xsl:with-param name="id" select="../id" />
                    </xsl:call-template>
                </parentGroupId>
            </xsl:if>
            <name><xsl:value-of select="name"/></name>
            <position>
                <x><xsl:value-of select="position/@x"/></x>
                <y><xsl:value-of select="position/@y"/></y>
            </position>
            <comments><xsl:value-of select="comment"/></comments>
            <contents>
                <xsl:apply-templates select="connection"/>
                <xsl:apply-templates select="label"/>
                <xsl:apply-templates select="processGroup"/>
                <xsl:apply-templates select="processor"/>
            </contents>
            <!-- TODO -->
            <variables/>
        </processGroups>
    </xsl:template>

    <xsl:template name="id">
        <xsl:param name="id"/>
        <xsl:value-of select="concat(substring($id, 0, 20), '0000-000000000000')"/>
    </xsl:template>

    <xsl:template match="styles">
        <style>
            <xsl:for-each select="style">
                <xsl:sort select="@name" />
                <entry>
                    <key><xsl:value-of select="@name"/></key>
                    <value><xsl:value-of select="text()"/></value>
                </entry>
            </xsl:for-each>
        </style>
    </xsl:template>
        
    <xsl:template match="label">
        <labels>
            <id>
                <xsl:call-template name="id">
                    <xsl:with-param name="id" select="id" />
                </xsl:call-template>
            </id>
            <parentGroupId>
                <xsl:call-template name="id">
                    <xsl:with-param name="id" select="../id" />
                </xsl:call-template>
            </parentGroupId>
            <label><xsl:value-of select="value"/></label>
            <position>
                <x><xsl:value-of select="position/@x"/></x>
                <y><xsl:value-of select="position/@y"/></y>
            </position>
            <xsl:apply-templates select="styles"/>
            <width><xsl:value-of select="size/@width"/></width>
            <height><xsl:value-of select="size/@height"/></height>
        </labels>
    </xsl:template>
        
    <xsl:template match="connection">
        <connections>
            <id>
                <xsl:call-template name="id">
                    <xsl:with-param name="id" select="id" />
                </xsl:call-template>
            </id>
            <parentGroupId>
                <xsl:call-template name="id">
                    <xsl:with-param name="id" select="../id" />
                </xsl:call-template>
            </parentGroupId>
            <name><xsl:value-of select="name"/></name>
            <labelIndex><xsl:value-of select="labelIndex"/></labelIndex>
            <zIndex><xsl:value-of select="zIndex"/></zIndex>

            <source>
                <groupId>
                    <xsl:call-template name="id">
                        <xsl:with-param name="id" select="sourceGroupId" />
                    </xsl:call-template>
                </groupId>
                <id>
                    <xsl:call-template name="id">
                        <xsl:with-param name="id" select="sourceId" />
                    </xsl:call-template>
                </id>
                <type><xsl:value-of select="sourceType"/></type>
            </source>
            
            <destination>
                <groupId>
                    <xsl:call-template name="id">
                        <xsl:with-param name="id" select="destinationGroupId" />
                    </xsl:call-template>
                </groupId>
                <id>
                    <xsl:call-template name="id">
                        <xsl:with-param name="id" select="destinationId" />
                    </xsl:call-template>
                </id>
                <type><xsl:value-of select="destinationType"/></type>
            </destination>
            
            <selectedRelationships><xsl:value-of select="relationship"/></selectedRelationships>
            <backPressureObjectThreshold><xsl:value-of select="maxWorkQueueSize"/></backPressureObjectThreshold>
            <backPressureDataSizeThreshold><xsl:value-of select="maxWorkQueueDataSize"/></backPressureDataSizeThreshold>
            <flowFileExpiration><xsl:value-of select="flowFileExpiration"/></flowFileExpiration>
        </connections>
    </xsl:template>
    
    <xsl:template match="processor">
        <processors>
            <id>
                <xsl:call-template name="id">
                    <xsl:with-param name="id" select="id" />
                </xsl:call-template>
            </id>
            <parentGroupId>
                <xsl:call-template name="id">
                    <xsl:with-param name="id" select="../id" />
                </xsl:call-template>
            </parentGroupId>
            <name><xsl:value-of select="name"/></name>
            <position>
                <x><xsl:value-of select="position/@x"/></x>
                <y><xsl:value-of select="position/@y"/></y>
            </position>
            <xsl:apply-templates select="styles"/>
            <type><xsl:value-of select="class"/></type>
            
            <bundle>
                <artifact><xsl:value-of select="bundle/artifact"/></artifact>
                <group><xsl:value-of select="bundle/group"/></group>
                <version><xsl:value-of select="bundle/version"/></version>
            </bundle>
            
            <config>
                <bulletinLevel><xsl:value-of select="bulletinLevel"/></bulletinLevel>
                <comments><xsl:value-of select="comment"/></comments>
                <concurrentlySchedulableTaskCount><xsl:value-of select="maxConcurrentTasks"/></concurrentlySchedulableTaskCount>
                <executionNode><xsl:value-of select="executionNode"/></executionNode>
                <lossTolerant><xsl:value-of select="lossTolerant"/></lossTolerant>
                <penaltyDuration><xsl:value-of select="penalizationPeriod"/></penaltyDuration>
                <properties>
                    <xsl:for-each select="property">
                        <xsl:sort select="name" />
                        <entry>
                            <key><xsl:value-of select="name"/></key>
                            <xsl:if test="value">
                                <value><xsl:value-of select="value"/></value>
                            </xsl:if>
                        </entry>
                    </xsl:for-each>
                </properties>
                <runDurationMillis><xsl:value-of select="runDurationNanos * 1e-6"/></runDurationMillis>
                <schedulingPeriod><xsl:value-of select="schedulingPeriod"/></schedulingPeriod>
                <schedulingStrategy><xsl:value-of select="schedulingStrategy"/></schedulingStrategy>
                <yieldDuration><xsl:value-of select="yieldPeriod"/></yieldDuration>
            </config>
            
            <xsl:for-each select="autoTerminatedRelationship">
                <xsl:sort select="." />
                <relationships>
                    <autoTerminate>true</autoTerminate>
                    <name><xsl:value-of select="."/></name>
                </relationships>
            </xsl:for-each>

            <state><xsl:value-of select="scheduledState"/></state>
        </processors>
    </xsl:template>
    
</xsl:stylesheet>
